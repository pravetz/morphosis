$(document).ready(function () {
    $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('input[name="_token"]').val()
        }
    });

    Chart.defaults.global.legend = false;
    Chart.defaults.global.responsive = false;

    var ctx = $('#myChart');
    var data = {
        labels: [],
        datasets: [{
            data: [1, 1, 1],
            backgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56",
                "#D01616",
                "#"
            ],
            hoverBackgroundColor: [
                "#FF6384",
                "#36A2EB",
                "#FFCE56"
            ]
        }]
    };


    $.ajax({
        type: 'POST',
        url: '/sections/all',
        success: function(response) {
            
            for(var i in response){
                data.labels.push(response[i].name);
            }
            
            var myPieChart = new Chart(ctx, {
                type: 'pie',
                data: data
            });
            
        }
    });
});
