function initMap() {
    var zoom = 4;

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 47.572745, lng: 11.113121},
        zoom: zoom
    });

    var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);

    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);

    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });


    // google.maps.event.addListener(searchBox, 'places_changed', function() {
    //     var newMarkers = []; //a temp variable to store the markers that should be visible
    //     if (input.value === 0) {
    //         marker.clear();
    //     }
    //     var bounds = new google.maps.LatLngBounds();
    //     for (var i = 0; i < newMarkers.length; i++) {
    //         bounds.extend(newMarkers[i].getPosition());
    //     }
    //     markerCluster.clearMarkers();
    //     markerCluster.addMarkers(newMarkers);
    //     map.fitBounds(bounds);
    // });

    autocomplete.addListener('place_changed', function () {


        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();

        appGlobals.currLocation = place.address_components;

        if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        $(function () {
            $(document).ready(function () {
                $("input").keypress(function () {
                    // if (e.keyCode == 8 || e.keyCode == 46) {
                       console.log("He");
                    // }
                });
            });

        });

        $(function () {
            $(document).ready(function () {
                $(".glyphicon-remove").click(function () {
                    $("#pac-input").val(null);
                });
            });
        });

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }

        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);

        change();
    });

}

$(() => {
    var languages = $('.language');
    languages.on('change', change);
})


function change() {

    var output = {};

    output.languages = getActiveLanguages();
    output.location = getCurrentLocation();
    output.sections = getActiveSections();

    console.log(output);

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.ajax({
        'url': '/articles/all',
        'method': 'POST',
        'processData': true,
        'data': output,
        'success': function (response) {

            console.log(response);

            //console.log(response);
            if (response.articles.length > 0)
                $('#table').html(response.table);
            else
                $('#table').html("");
        },
        'error': function (error) {

            document.write(error.responseText);

            console.log(error);
        }
    });
}
