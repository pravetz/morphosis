function initMap() {
    var zoom = 4;

    var lat = parseFloat(document.getElementById('latitude').value);
    var lng = parseFloat(document.getElementById('longitude').value);
    var myLatLng = {lat: lat, lng: lng};

    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 47.572745, lng: 11.113121},
        zoom: zoom
    });

    var card = document.getElementById('pac-card');
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);

    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);

    var marker = new google.maps.Marker({
        position: myLatLng,
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    autocomplete.addListener('place_changed', function () {

        infowindow.close();
        marker.setVisible(false);
        var place = autocomplete.getPlace();

        // appGlobals.currLocation = place.address_components;

        if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        document.getElementById('latitude').value = marker.getPosition().lat();
        document.getElementById('longitude').value = marker.getPosition().lng();

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-icon'].src = place.icon;
        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);

        change();
    });

    map.addListener('click', function(event) {
        if(marker) {
            marker.setPosition(event.latLng);
        } else {
            marker = new google.maps.Marker({
                position: document.getElementById('latitude').value(),
                map: map
            });
        }
        var lat = marker.getPosition().lat();
        var lng = marker.getPosition().lng();
        var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" + lat + ", " + lng + "&sensor=false";

        $.getJSON(url, function (data) {
            var address = data.results[0].formatted_address;
            document.getElementById("pac-input").value = address;
        });
        document.getElementById('latitude').value = marker.getPosition().lat();
        document.getElementById('longitude').value = marker.getPosition().lng();

    });
}
