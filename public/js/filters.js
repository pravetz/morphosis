'use strict';

var appGlobals = {

    currLocation : {},
};


$(function(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
})

/**
 * getActiveLanguages function
 * returns the active languages from the table
 *
 * @return JSON
 */
function getActiveLanguages(){

    var languages = [];

    $("#checkboxes").find("input:checked").each(function (i, ob) {

        languages.push($(ob).attr('data-id'));
    });

    return languages;
}


function getCurrentLocation(){

    if(typeof appGlobals.currLocation == "null"){
        return [];
    }
    return appGlobals.currLocation;
}

function getActiveSections(){

  var sections = [];

  $("g#container").find("path[checked=true]").each(function (i, ob) {

    sections.push($(ob).attr("id"));
  })

  return sections;
}
