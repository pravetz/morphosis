@extends('layouts.panel')

@section('styles')
    <link rel="stylesheet" href="/css/school.css">
@endsection

@section('body')


    <form class="form-horizontal" role="form" method="POST">

        {{ csrf_field() }}


        <div class="form-group">
            <label class="control-label col-sm-2" for="name">Name:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="name" id="name"
                       placeholder="Enter name"
                       value="{!! $errors->has('name') ? old('name') : (isset($school) ? $school->name : null) !!}">

                {!! $errors->first('name') !!}
            </div>

        </div>
        <div class="form-group" style="display: none">
            <label class="control-label col-sm-2" for="latitude">Latitude:</label>
            <div class="col-sm-10">
                <input type="hidden" class="form-control" name="latitude" id="latitude"
                       placeholder="Enter latitude"
                       value="{!! $errors->has('latitude') ? old('latitude') : (isset($school) ? $school->location->latitude : null) !!}">
            </div>
        </div>
        <div class="form-group" style="display: none">
            <label class="control-label col-sm-2" for="longitude">Longitude:</label>
            <div class="col-sm-10">
                <input type="hidden" class="form-control" name="longitude" id="longitude"
                       placeholder="Enter longitude"
                       value="{!! $errors->has('longitude') ? old('longitude') : (isset($school) ? $school->location->longitude : null) !!}">
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-sm-2" for="address">Address:</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="address" id="pac-input"
                       placeholder="Enter address"
                       value="{!! $errors->has('address') ? old('address') : (isset($school) ? $school->location->address : null) !!}">

                {!! $errors->first('address') !!}

            </div>
        </div>

        <div class="form-group" id="map"></div>


        <div class="form-group">
            <div class="col-md-12" style="margin: auto">
                <input type="submit" value="{!! isset($school) ? 'Edit' : 'Create' !!}" class="btn btn-success">
                <a href="/schools" class="btn btn-default">Cancel</a>
            </div>
        </div>
    </form>

@endsection

@section('scripts')
    <script
            src="/js/schoolMap.js"></script>

    {{--<script--}}
    {{--src="/js/indexMap.js"></script>--}}
    {{--<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>--}}
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOYsmoUUUt7FnWJjMkrH3jxd5bLDxnzr4&libraries=places&callback=initMap&language=en"
            async defer></script>

    {{--<script type="text/javascript" src="js/jquery-1.8.3.min.js"></script>--}}

@endsection
