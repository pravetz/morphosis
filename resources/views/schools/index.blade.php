@extends('layouts.panel')

@section('body')


    <a href="/schools/create" class="btn btn-success pull-right">Create new school</a>
    @if($schools->isEmpty())
        <h4>You don't have any schools yet.</h4>
    @else
        <table class="table table-hover">
            <thead>
            <tr>
                <th>School name</th>
                <th>Address</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($schools as $school)
                <tr>
                    <td><a href="/schools/show/{{ $school->id }}">{{ $school->name }}</a></td>
                    <td>{{ $school->location->address }}</td>
                    <td>
                        @if (Auth::user()->role == 1 || Auth::user()->id == $school->user_id)
                            {{--<a href="/admin">--}}
                            <a href="/schools/edit/{{ $school->id }}" class="btn btn-primary">Edit</a>
                            {{--</a>--}}
                        @endif

                    </td>
                </tr>

            @endforeach
            </tbody>
        </table>
        {!! $schools->render() !!}
    @endif

@endsection
