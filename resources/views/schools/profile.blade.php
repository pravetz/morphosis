@extends('layouts.panel')

@section('body')
    <!-- Data of the school -->

    <div class="panel-body text-center">
        @if(session('response'))
            <div class="alert alert-{{ session('response.status') }}">
                {{ session('response.message') }}
            </div>
        @endif
        <h1>{{ $school->name }}</h1>
        {{--            <h5>{{ $school->location->latitude }}</h5>--}}
        {{--            <h5>{{ $school->location->longitude }}</h5>--}}
        <a href="/schools" class="btn btn-default">Go back</a>
        <a href="/document/add/{{ $school->id }}" class="btn btn-success">Add documents</a>
    </div>

    @if($school->articles->isEmpty())
        <div class="alert alert-danger">
            This school doesn't have any documents yet!
        </div>
    @else
        <h2 class="text-center">Your documents</h2>
        <div class="panel-body">
            <table class="table table-hover" style="margin-right: 15px;">
                <thead>
                <tr>
                    <th>Section</th>
                    <th>Title</th>
                    <th>Language</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>

                @foreach($school->articles as $article)
                    @foreach($article->fields as $field)
                    <tr>
                        <td>{{ $article->section_id }}</td>
                        <td>{{ $field->title }}</td>
                        <td>{{ $field->language->name }}</td>
                        <td>
                            @if (Auth::user()->role == 1 || Auth::user()->id == $school->user_id)
																<a href="/document/edit/{{ $article->id }}" class="btn btn-primary">Edit</a>
																<a href="/document/translation/add/{{ $field->id }}" class="btn btn-primary">Add translation</a>
                            @endif

                        </td>
                    </tr>
                    @endforeach
                @endforeach

                </tbody>

            </table>
        </div>
    @endif

@endsection