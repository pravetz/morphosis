<div class="container" style="margin-top: 100px;">
    <div class="row">
        <div class="panel panel-default">
            <div class="panel-body">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Title</th>
                        {{--<th>Short Description</th>--}}
                        {{--<th>Long Description</th>--}}
                        <th>Language</th>
                        <th>Section</th>
                        <th>School</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)

                        <tr class="clickToExpand" expandId="{!! $article->id !!}">
                            {{--<tr data-toggle="collapse" data-target="#accordion" class="clickable">--}}
                            <td>{!! $article->title !!}</td>
                            <td>{!! $article->language->name !!}</td>
                            <td>{!! $article->article ? $article->article->section->name : null !!}</td>
                            <td>{!! $article->article ? $article->article->school->name : null !!}</td>
                        </tr>

                        <tr>
                            <td colspan="4" style="display: none" class="clickable" expandId="{!! $article->id !!}">
                                <div>
                                    <div class="col-md-6">
                                        <h4>Short Description</h4>
                                        <p>{!! $article->text_short !!}</p>
                                    </div>

                                    <div class="col-md-6">
                                        <h4>Long Description</h4>
                                        <p>{!! $article->text_long !!}</p>
                                    </div>
                                </div>
                            </td>
                        </tr>

                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script>
    $(document).ready(function () {
        $(".clickToExpand").click(function () {
            $(".clickable[expandId='" + $(this).attr('expandId') + "']").toggle("slow");
        });
    });
</script>