@extends('layouts.app')


@section('styles')
    <link rel="stylesheet" href="/css/index.css">
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="row">
        <div class="col-md-3">
            <table id="checkboxes" class="table">
                <thead>
                <tr>
                    <th>Language</th>
                    <th></th>
                </tr>
                </thead>
                @foreach($languages as $language)
                    <tbody>
                        <tr>
                            <td>{{ $language['name'] }}</td>
                            <td>
                                <input checked class="language" type="checkbox" data-toggle="toggle" data-onstyle="success"
                                       data-offstyle="danger" data-id="{{ $language['id'] }}">
                            </td>
                        </tr>
                    </tbody>

                @endforeach
            </table>
        </div>

        <div class="col-md-6">
            <div id="main">
                <div id="sequence"></div>
                <div id="chart">

                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div id="pac-container">
                <input id="pac-input" type="text" placeholder="Enter a location">
                <span class="glyphicon glyphicon-remove" style="position: absolute; top: 30px; right: 30px; z-index: 99;"></span>
            </div>
            <div id="map"></div>
            <div id="infowindow-content">
                <img src="" width="16" height="16" id="place-icon">
                <span id="place-name" class="title"></span><br>
                <span id="place-address"></span>
            </div>
        </div>
    </div>
    <div id="table">

    </div>

@endsection

@section('scripts')

    <script>
//         $(document).ready(function(){
//             $(".glyphicon-remove").click(function(){
//                 $("#pac-input").val(null);
//                 $('#map').gmap('clearMarkers');
//             });
//         });
    </script>

    <script src="/js/filters.js"></script>
    <script src="/js/indexMap.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.17/d3.min.js"></script>
    <script src="/js/d3.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDOYsmoUUUt7FnWJjMkrH3jxd5bLDxnzr4&libraries=places&callback=initMap&language=en"
            async defer></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
@endsection
