@extends('layouts.panel')

@section('body')

<div class="col-md-6">
	<label for="title">Title:</label>
	<input disabled class="form-control" type="text" value="{!! $article_fields->title !!}"><br>

	<label for="text-short">Short description:</label>
	<textarea disabled class="form-control">{!! $article_fields->text_short !!}</textarea><br>

	<label for="text-long">Long description:</label>
	<textarea disabled class="form-control" id="text-long-disabled">{{ $article_fields->text_long }}</textarea><br>

	<label for="language">Language:</label>
	<input disabled class="form-control" value="{{ $article_fields->language->name }}">
</div>

<div class="col-md-6">
	<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
		{{ csrf_field() }}

		<label for="title">Title:</label>
		<input class="form-control" type="text" name="title" id="title" placeholder="Title..."
					 value="{!! $errors->has('title') ? old('title') : (isset($article_field) ? $article_field->title : null) !!}">
		{!! $errors->first('title') !!} <br>

		<label for="text-short">Short description:</label>
		<textarea class="form-control" name="text_short" id="text-short">{!! $errors->has('text_short') ? old('text_short') : (isset($article_field) ? $article_field->text_short : null) !!}</textarea>
		{!! $errors->first('text_short') !!}<br>

		<label for="text-long">Long description:</label>
		<textarea class="form-control" name="text_long" id="text-long">{!! $errors->has('text_long') ? old('text_long') : (isset($article_field) ? $article_field->text_long : null) !!}</textarea>
		{!! $errors->first('text_long') !!}<br>

		<label for="pdf">Add PDF File:</label>
		<input type="file" name="pdf" accept=".pdf">
		{!! $errors->first('pdf') !!}<br>


		<label for="language">Select language:</label>
		<select class="form-control" name="language_id" id="language">
				@foreach ($languages as $language)
						<option value="{{ $language->id  }}" {!! isset($article_field) ? ($article_field->language_id == $language->id ? 'selected' : null) : null !!}>{{ $language->name }}</option>
				@endforeach
		</select><br>

		<input class="btn btn-default" type="submit"  value="Add">
	</form>
</div>

@endsection

@section('scripts')
		<script src="/js/ckeditor/ckeditor.js"></script>
		<script src="/js/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
        $( '#text-long' ).ckeditor();
				$( '#text-long-disabled' ).ckeditor();
    </script>
@endsection