@extends('layouts.panel')

@section('styles')
    <link rel="stylesheet" href="/css/documents.css">
@endsection

@section('body')


    <h3>{!! isset($article) ? 'Edit' : 'Add' !!} documents to school</h3>
    {{--<form action="/document/postAdd/{{ $id }}" method="POST" class="form-horizontal"--}}
    <form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">

        {{ csrf_field() }}

        <label for="section">Select section:</label>
        <select class="form-control" name="section_id" id="section">
            @foreach ($sections as $section)
                <option value="{{ $section->id }}" {!! isset($article) ? ($article->section_id == $section->id ? 'selected' : null) : null !!}>{{ $section->name }}</option>
            @endforeach
        </select>

        <label for="title">Title:</label>
        <input class="form-control" type="text" name="title" id="title" placeholder="Title..."
               value="{!! $errors->has('title') ? old('title') : (isset($article_field) ? $article_field->title : null) !!}">
        {!! $errors->first('title') !!} <br>

        <label for="text-short">Short description:</label>
        <textarea class="form-control" name="text_short" id="text-short">{!! $errors->has('text_short') ? old('text_short') : (isset($article_field) ? $article_field->text_short : null) !!}</textarea>
        {!! $errors->first('text_short') !!}<br>

        <label for="text-long">Long description:</label>
        <textarea class="form-control" name="text_long" id="text-long">{!! $errors->has('text_long') ? old('text_long') : (isset($article_field) ? $article_field->text_long : null) !!}</textarea>
        {!! $errors->first('text_long') !!}<br>

        <label for="pdf">Add PDF File:</label>
        <input type="file" name="pdf" accept=".pdf">
        {!! $errors->first('pdf') !!}<br>


        <label for="language">Select language:</label>
        <select class="form-control" name="language_id" id="language">
            @foreach ($languages as $language)
                <option value="{{ $language->id  }}" {!! isset($article_field) ? ($article_field->language_id == $language->id ? 'selected' : null) : null !!}>{{ $language->name }}</option>
            @endforeach
        </select>

        <input class="btn btn-default" type="submit"  value="{!! isset($article) ? 'Edit' : 'Add' !!}">
    </form>

@endsection

@section('scripts')
    <script src="/js/ckeditor/ckeditor.js"></script>
		<script src="/js/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript">
        CKEDITOR.replace('text-long');
    </script>
@endsection
