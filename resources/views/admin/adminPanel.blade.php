@extends('layouts.panel')

@section('body')

    @if(session('response'))
        <div class="alert alert-{{ session('response.status') }}">
            {{ session('response.message') }}
        </div>
    @endif

    <h1>Users</h1>

    <table class="table table-hover">
        <thead>
        <tr>
            <th>Name</th>
            <th>E-mail</th>
            <th>Role</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if ($user->role == 0)
                        Normal
                    @elseif ($user->role == 1)
                        Administrator
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <hr>

    <h1>Sections</h1>

    <table class="table table-hover">
        <thead>
            <th>Id</th>
            <th>Name</th>
            <th></th>
        </thead>
        <tbody>
            @foreach($sections as $section)
                <tr>
                    <td>{{ $section->id }}</td>
                    <td>{{ $section->name }}</td>
                    <td>
                        <a href="/admin/sections/{{ $section->id }}/edit" class="btn btn-primary pull-left">Edit</a>
                        <form action="admin/sections/{{ $section->id }}" method="POST" class="pull-left">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            &nbsp;&nbsp;
                            <input type="submit" value="Delete" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <a href="/admin/sections/create" class="btn btn-lg btn-success pull-right">Add</a>

@endsection