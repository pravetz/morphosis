@extends('layouts.panel')

@section('body')

    <h2>{{ isset($section) ? 'Edit' : 'Create new' }} section</h2>
    <br>
    <form action="/admin/sections{{ isset($section) ? '/'.$section->id : null }}" method="POST">
        @if(isset($section))
            {{ method_field('PUT') }}
        @endif
        {{ csrf_field() }}
        <div class="form-group">
            <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name"
                @if(isset($section))
                    value="{{ $section->name }}"
                @else
                   placeholder="Name..."
                @endif
            >
        </div>
        <input type="submit" value="{{ isset($section) ? 'Save' : 'Create' }}" class="btn btn-success">
    </form>

@endsection