<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DefineForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schools', function (Blueprint $table){

          $table->foreign('user_id')
          ->references('id')->on('users')
          ->onDelete('cascade');

          $table->foreign('location_id')
          ->references('id')->on('locations')
          ->onDelete('cascade');

        });

        Schema::table('articles', function (Blueprint $table){

          $table->foreign('school_id')
          ->references('id')->on('schools')
          ->onDelete('cascade');

          $table->foreign('section_id')
          ->references('id')->on('sections')
          ->onDelete('cascade');

        });
        
        Schema::table('article_fields', function (Blueprint $table){

          $table->foreign('article_id')
          ->references('id')->on('articles')
          ->onDelete('cascade');

          $table->foreign('language_id')
          ->references('id')->on('languages')
          ->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
