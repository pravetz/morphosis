<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDatabase extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('schools', function (Blueprint $table){
          $table->increments('id');
          $table->string('name');
          $table->integer('user_id')->unsigned();
          $table->integer('location_id')->unsigned();
          $table->softDeletes();
          $table->timestamps();
        });
        
        Schema::create('sections', function (Blueprint $table){
          $table->increments('id');
          $table->string('name');
          $table->softDeletes();
          $table->timestamps();
        });
        
        Schema::create('articles', function (Blueprint $table){
          $table->increments('id');
          $table->integer('school_id')->unsigned();
          $table->integer('section_id')->unsigned();
          $table->softDeletes();
          $table->timestamps();
        });
        
        Schema::create('languages', function (Blueprint $table){
          $table->increments('id');
          $table->string('name');
          $table->softDeletes();
          $table->timestamps();
        });
        
        Schema::create('locations', function (Blueprint $table){
          $table->increments('id');
          $table->double('latitude', 8, 5);
          $table->double('longitude', 8, 5);
          $table->softDeletes();
          $table->timestamps();
        }); 
        
        Schema::create('article_fields', function (Blueprint $table){
          $table->increments('id');
          $table->string('title', 60);
          $table->text('text_short');
          $table->text('text_long');
          $table->integer('article_id')->unsigned();
          $table->integer('language_id')->unsigned();
          $table->softDeletes();
          $table->timestamps();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::dropIfExists('schools');
        Schema::dropIfExists('sections');
        Schema::dropIfExists('articles');
        Schema::dropIfExists('languages');
        Schema::dropIfExists('locations');
        Schema::dropIfExists('article_fields');

    }
}
