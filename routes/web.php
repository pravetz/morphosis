<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/



//Edit a document


Route::group(['middleware' => 'admin', 'namespace' => 'Admin', 'prefix' => 'admin'], function () {

    Route::get('/', 'AdminController@adminPanel');
    Route::resource('sections', 'SectionController', ['except' => [
        'show', 'index'
    ]]);
});

Route::group(['middleware' => 'isLogged'], function () {

    Route::get('/', 'HomeController@index');

    Route::post('/articles/all', 'ArticlesController@getArticles');

    Route::get('/sections/all', 'SectionController@getSections');

    Route::get('/schools', 'SchoolController@index');
    Route::get('/schools/create', 'SchoolController@create');
    Route::post('/schools/create', 'SchoolController@postCreate');
    Route::get('/schools/show/{id}', 'SchoolController@profile');

    Route::get('/schools/edit/{id}', 'SchoolController@edit');
    Route::post('/schools/edit/{id}', 'SchoolController@postUpdate');

    Route::get('/document/add/{id}', 'DocumentController@add');
    Route::post('/document/add/{id}', 'DocumentController@postAdd');

    Route::get('/document/translation/add/{id}', 'DocumentController@translation');
    Route::post('/document/translation/add/{id}', 'DocumentController@postTranslation');

    Route::get('/document/edit/{id}', 'DocumentController@edit');
    Route::post('/document/edit/{id}', 'DocumentController@postUpdate');
});

Auth::routes();


Route::get('migrate', function(){
    Artisan::call('migrate');

    echo 'migration done';
});