<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'name', 'user_id', 'location_id',
    ];
    
    protected $visible = [
        'name', 'user_id', 'location_id',
    ];
    
    public function location(){
        
        return $this->belongsTo('App\Location');
    }

    public function articles(){
        return $this->hasMany(Article::class);
    }
}
