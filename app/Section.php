<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $visible = [
        'name', 'id'
    ];

    protected $fillable = [
        'name'
    ];

    public function article() {
        return $this->belongsTo('App\Article');
    }

}
