<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{

    protected $fillable = [
        'section_id', 'school_id',
    ];

    public function school() {
      return $this->belongsTo('App\School');
    }

    public function section() {
      return $this->belongsTo('App\Section');
    }

    public function field(){

        return $this->hasOne(Article_field::class);
    }

    public function fields(){

        return $this->hasMany(Article_field::class);
    }
}
