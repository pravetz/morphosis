<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article_field extends Model
{

    protected $visible = [
        'id', 'title', 'text_short', 'text_long', 'article_id', 'language_id',
    ];

    public $fillable = [
        'title', 'text_short', 'text_long', 'article_id', 'language_id', 'file_name',
    ];

    public function article() {
      return $this->belongsTo('App\Article');
    }

    public function language() {
      return $this->belongsTo('App\Language');
    }
}
