<?php
/**
 * Created by PhpStorm.
 * User: radoslavstefanov
 * Date: 4/5/17
 * Time: 15:56
 */

namespace App\Core;


class System
{


    public static function getURLResponse($url)
    {

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    public static function getJsonFromURL($url)
    {

        $response = self::getURLResponse($url);

        return json_decode($response);
    }

    public static function getAddressFromCoordinates(float $lat, float $lng)
    {

        $url = "http://maps.googleapis.com/maps/api/geocode/json?latlng=" . $lat . "," . $lng . "&sensor=false";

        $json = self::getJsonFromURL($url);

        if($json->status == "OK"){

            return $json->results[0]->address_components;
        }

        return false;


    }

    public static function exportAddresses($json)
    {

        $response = [];

        foreach ($json as $location) {

            $response[] = [
                "name" => $location['long_name'],
                "type" => $location['types'][0]
            ];
        }

        return $response;
    }

    public static function getLowestAddress($json)
    {

        return self::exportAddresses($json)[0];
    }

    public static function checkIfInsideLocation($addresses, $pattern){

        foreach($addresses as $address){

            if($address->types[0] == $pattern['type'] && $address->long_name == $pattern['name']){

                return true;
            }
        }

        return false;
    }
}