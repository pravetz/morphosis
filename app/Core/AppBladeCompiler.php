<?php

namespace App\Core;

use Illuminate\View\Compilers\BladeCompiler;
use Config;

/**
 * Created by PhpStorm.
 * User: radoslavstefanov
 * Date: 3/30/17
 * Time: 13:53
 */
class AppBladeCompiler extends BladeCompiler
{

    public function isExpired($path)
    {

        if ( ! \Config::get('view.cache'))
        {
            return true;
        }

        return parent::isExpired($path);
    }
}