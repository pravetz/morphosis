<?php

namespace App\Http\Controllers;

use App\Section;
use App\School;
use App\Language;
use App\Article;
use App\Article_field;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $languages = Language::get();
        $sections = Section::get();

        return view('index', compact('sections', 'languages'));
    }

    public function home()
    {
        return view('home');
    }
}
