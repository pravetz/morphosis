<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddDocument;
use App\Section;
use App\School;
use Auth;
use App\Language;
use App\Article;
use App\Article_field;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;

class DocumentController extends Controller
{

    public function add($id)
    {

        $languages = Language::all();
        $sections = Section::all();

        return view('documents.add', compact(['id', 'sections', 'languages']));
    }

    public function postAdd($id, AddDocument $request)
    {

        if ($request->hasFile('pdf')) {
            $pdf = $request->file('pdf');

            $file = Storage::putFile('pdf', $pdf);
            $arr = explode("/", $file);
        }

        $article = Article::create([
            'section_id' => $request->input('section_id'),
            'school_id' => $id
        ]);

        $article_field = Article_field::create(
            array_merge(
                $request->all(),
                [
                    'article_id' => $article->id,
                    'language_id' => $request->input('language_id'),
                    'file_name' => $arr[1] ?? NULL
                ]
            )
        );

        return redirect('/schools/show/' . $id)->with('response', [
            'status' => 'success',
            'message' => 'You have added your documents successfully!'
        ]);
    }

    public function edit($id)
    {


        $languages = Language::all();
        $sections = Section::all();
        $article = Article::find($id);

        if ($article) {
            //if (Auth::user()->role == 1) {
                return view('documents.add')->with([
                    'sections' => $sections,
                    'languages' => $languages,
                    'article' => $article,
                    'article_field' => $article->field
                ]);
            //}
        }
        return redirect("/schools")->with('response', [
            'status' => 'danger',
            'message' => 'The document was not found!'
        ]);
    }

    public function postUpdate($id, AddDocument $request)
    {

        $article = Article::find($id);
        $article_fields = $article->fields();

//        if (($article && Auth::user()->role == 1) || Auth::user()->id == $article->user_id) {
        if ($article) {
            $article->update($request->all());
            $article_fields->update($request->only('title', 'text_short', 'text_long', 'language_id', 'file_name'));

            return redirect("/schools/show/" . $article->school->id)->with('response', [
                'status' => 'success',
                'message' => 'The document was successfully updated!'
            ]);
//        }
        }
        return redirect("/schools")->with('response', [
            'status' => 'danger',
            'message' => 'The document was not found!'
        ]);
    }

		public function translation($id)
		{
			$article_fields = Article_field::find($id);
			$languages = Language::all();

			return view('documents.translation', compact('article_fields', 'languages'));
		}

		public function postTranslation($id, AddDocument $request)
		{
			$old_article_fields = Article_field::find($id);
			$article_id = $old_article_fields->article_id;

			if ($request->hasFile('pdf')) {
					$pdf = $request->file('pdf');

					$file = Storage::putFile('pdf', $pdf);
					$arr = explode("/", $file);
			}

			$new_article_field = Article_field::create(
					array_merge(
							$request->all(),
							[
									'article_id' => $article_id,
									'language_id' => $request->input('language_id'),
									'file_name' => $arr[1] ?? NULL
							]
					)
			);

			return redirect('/schools/show/' . $old_article_fields->article->school_id)->with('response', [
					'status' => 'success',
					'message' => 'You have added your documents successfully!'
			]);
		}
}
