<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSchool;
use Illuminate\Http\Request;
use Auth;
use App\School;
use App\Location;
use App\User;
use App\Article;
use App\Article_field;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;

class SchoolController extends Controller
{
	
    public function index()
    {

        return view('schools.index', [
            'schools' => School::where('user_id', Auth::user()->id)->paginate(10)
        ]);

    }

    public function create()
    {
        return view('schools.create');
    }

    public function postCreate(CreateSchool $request)
    {
//        $schoolName = School::create($request->all());
        $location = Location::create($request->all());
//        $address = "Adres4ence";

        $school = School::create(
            array_merge(
                $request->all(),
                [
                    'user_id' => Auth::user()->id,
                    'location_id' => $location->id
                ]
            )
        );


        return redirect('/schools')->with('response', [
            'status' => 'success',
            'message' => 'The school was successfully created!'
        ]);
    }

    public function profile($id)
    {

        $school = School::find($id);
//        $articles = Article::where('school_id', $id)->get();
//        $articles->toArray();
        $article_fields = Article_field::all();


        return view('schools/profile', compact(
            'school'
        ));
    }

    public function edit($id)
    {

        $school = School::find($id);
        if ($school) {
            if (($school && Auth::user()->role == 1) || Auth::user()->id == $school->user_id) {
                return view('schools.create')->with([
                    'school' => $school
                ]);
            }
        }

        return redirect('/schools')->with('response', [
            'status' => 'danger',
            'message' => 'The school was not found!'
        ]);
    }

    public function postUpdate($id, CreateSchool $request)
    {

        $school = School::find($id);

        $school->location()->update($request->all());

        $school->update($request->all());

        return redirect('/schools')->with('response', [
            'status' => 'success',
            'message' => 'The school was successfully updated!'
        ]);

    }

}
