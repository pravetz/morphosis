<?php

namespace App\Http\Controllers;

use App\Section;
use App\Article;
use Illuminate\Http\Request;

class SectionController extends Controller
{

    public function getSections(){

        $sections = Section::all();
        $articles = [];
        $formatted = [
            'name' => 'Sections',
            'children' => []
        ];

        foreach($sections as $section) {
            array_push($articles, Article::where('section_id', $section->id)->get());
        }

        for($i = 0; $i < count($sections); $i++) {
            array_push($formatted['children'], [
                'id' => $sections[$i]->id,
                'name' => $sections[$i]->name,
                'size' => count($articles[$i]),
                'children' => [
                    'name' => 'Test',
                    'size' => random_int(0, 100)
                ]
            ]);
        }

        return $formatted;
    }
}
