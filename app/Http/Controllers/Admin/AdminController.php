<?php

namespace App\Http\Controllers\Admin;

use App\Section;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    
    public function adminPanel()
    {
        
        $users = User::all();
        $sections = Section::all();
        return view('admin.adminPanel', compact(['users', 'sections']));
    }
}
