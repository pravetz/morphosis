<?php

namespace App\Http\Controllers;

use App\Core\System;
use App\Location;
use Illuminate\Http\Request;
use App\Article_field;
use App\Article;
use App\Language;
use App\Section;
use App\School;

class ArticlesController extends Controller
{

    public function getArticles(Request $request)
    {

        $articles = Article_field::where('id', '>=', 1);

        $languages_id = $request->input('languages') ?? [];
        $sections_id = $request->input('sections') ?? [];
        $sections = Section::whereIn('id', $sections_id)->get();
        $chart = [
          'name' => 'Sections',
          'children' => []
        ];

        if($request->has('languages')){
          $articles = $articles->whereIn('language_id', $languages_id);
        }

        //$articlesAll = Article::whereIn('section_id', $sections_id)->get();

        if($request->has('sections')){

          $articleIds = [];
          $articlesHere = Article::whereIn('section_id', $sections_id)->get();

          foreach($articlesHere as $article){

            foreach($article->fields as $field){

              $articleIds[] = $field->id;
            }
          }


          $articles = $articles->whereIn('id', $articleIds);

        }

        if ($request->has('location')) {


            $locations = Location::all();
            $allowedLocations = [];

            $originalLocation = System::getLowestAddress($request->input('location'));



            foreach ($locations as $location) {

                $address = System::getAddressFromCoordinates($location->latitude, $location->longitude);

                if($address){

                    $isInside = System::checkIfInsideLocation($address, $originalLocation);

                    if ($isInside) {
                        foreach ($location->school->articles as $article){
                            $allowedLocations[] = $article->field->id;
                        }
                    }

                }

            }

            $articles = $articles->whereIn('id', $allowedLocations);

        }

        $articles = $articles->get();

        $articlesAll = [];

        foreach($articles as $article_field) {
          array_push($articlesAll, $article_field->article);
        }

        foreach($sections as $section) {
          array_push($chart['children'], [
            'id' => $section->id,
            'name' => $section->name,
            'size' => count(Article::where('section_id', $section->id)->get()),
						'children' => [
								'name' => 'Test',
								'size' => random_int(0, 100)
						]
          ]);
        }

        $leftoverSections = Section::whereNotIn('id', $sections_id)->get();
        foreach($leftoverSections as $section) {
          array_push($chart['children'], [
            'id' => $section->id,
            'name' => $section->name,
            'size' => 0,
						'children' => [
								'name' => 'Test',
								'size' => random_int(0, 100)
						]
          ]);
        }

        $table = view('ajax.articlesTable', compact('articles'))->render();

        return compact('table', 'chart', 'articles');
    }
}
