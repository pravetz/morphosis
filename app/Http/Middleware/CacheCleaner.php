<?php

namespace App\Http\Middleware;

use Closure;
use App;

class CacheCleaner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {


        app()->singleton('blade.compiler', function($app)
        {
            $cache = $app['path.storage'].'/framework/views';

            return new App\Core\AppBladeCompiler($app['files'], $cache);
        });


        return $next($request);

    }
}
