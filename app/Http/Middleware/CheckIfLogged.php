<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIfLogged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->intended('/login')->with('response', [
                'type' => 'danger',
                'text' => 'Please, log in first to complete your action'
            ]);
        }

        return $next($request);
    }
}
