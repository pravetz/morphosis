<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use App\School;
use Auth;

class CreateSchool extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
				if(Request::segment(2) == "edit") {
					$id = Request::segment(3);
					$school = School::find($id);

	        return ($school && Auth::user()->role == 1) || Auth::user()->id == $school->user_id;
				}

				return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|min:3|max:255',
            'address' => 'required'
        ];
    }
}
