<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    protected $fillable = [
        'latitude', 'longitude', 'address'
    ];
    
    protected $visible = [
        'latitude', 'longitude', 'id', 'address'
    ];

    public function school(){
        return $this->hasOne(School::class);
    }
}
